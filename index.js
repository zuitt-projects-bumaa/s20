//alert("hellowwww");

/* JSON Objects (JavaScript Object Notation)
	- can also be used in other programming languages
	- different from JavaScript objects
	- used for serializing different data types into bytes
				- serialization is the process of converting data types into series of bytes for easier transmission/ transfer
				(esp in binary environment - backend/ server)
				- bytes are units of information that a computer processes to perform different tasks
	- uses double quotes for property name 
		e.g. let person = {
				"name" : "Yoon",
				"birthday" : "March, 24, 1993"
		};
	- common in servers
	- serves a great purpose in the transmission of data from frontend to backend

*/ 

/*
{
	"city" : "Quezon City", 
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/
// Json Objects can also be JSON Arrays
/*
"cities" : [
	{
		"city" : "Quezon City",
		"Province" : "Metro Manila",
		"country" : "Philippines"
	}
	{
		"city" : "Cebu City",
		"Province" : "Cebu",
		"country" : "Philippines"
	}
];
*/
// JSON Methods
	// - JSON objects contain methods for parsing and converting data into stringified JSON --impt for backroom like URL request (e.g. Postman can fully read JSON Objects)

	let batchesArr = [
		{batchName: "Batch 169"},
		{batchName: "Batch 170"},
	];

	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name: "Luke",
		age: 45,
		address: {
			city: "Manila",
			country: "Philippines"
		}
	});

	console.log(data);


	// Using stringify method with variables 

	// User details

	let firstName = prompt("First Name:");
	let lastName = prompt("Last Name:");
	let age = prompt("Age:");
	let address = {
		city: prompt("City:"),
		country: prompt("Country:")
	};
 

let data2 = JSON.stringify({
 // property : variable
	firstName : firstName,
	lastName : lastName,
	age : age, 
	address : address
}); 

console.log(data2);

// converting Stringified objects to JavaScript objects 

let batchesJSON = `[
	{
		"batchName" : "Batch 169"
	},
	{
		"batchName" : "Batch 170"
	}
]`



console.log(JSON.parse(batchesJSON));




















